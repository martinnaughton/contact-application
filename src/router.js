import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import List from './views/List.vue'
import Add from './views/Add.vue'
import Edit from './views/Edit.vue'
import Contact from './components/Contact.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/list',
      name: 'list',
      component: List
    },
    {
      path: '/add',
      name: 'add',
      component: Add
    },
    {
      path: '/edit',
      name: 'edit',
      component: Edit
    },
    {
      path: '/contact/:id',
      name: 'contact',
      component: Contact,
      params: true
    }
  ]
})
